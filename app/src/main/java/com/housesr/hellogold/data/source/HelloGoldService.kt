package com.housesr.hellogold.data.source

import com.google.gson.Gson
import com.housesr.hellogold.BuildConfig
import com.housesr.hellogold.data.model.RegisterResponse
import com.housesr.hellogold.data.model.SpotPriceResponse
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface HelloGoldService {

    @FormUrlEncoded
    @POST("api/v3/users/register.json")
    fun register(
        @Field("email") email: String,
        @Field("uuid") uuid: String,
        @Field("data") data: String,
        @Field("tnc") tnc: Boolean
    ): Single<RegisterResponse>

    @GET("api/v2/spot_price.json")
    fun spotPrice(): Single<SpotPriceResponse>

    companion object {

        @Volatile
        private var instance: HelloGoldService? = null

        fun getInstance(): HelloGoldService =
            instance ?: synchronized(this) {
                instance ?: init()
                    .also { instance = it }
            }

        private fun init(): HelloGoldService {
            val okHttpClient = OkHttpClient.Builder().apply {
                if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor()
                        .apply { level = HttpLoggingInterceptor.Level.BODY }
                        .also { addInterceptor(it) }
                }
            }.build()
            return Retrofit.Builder()
                .baseUrl("https://staging.hellogold.com/")
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(HelloGoldService::class.java)
        }
    }
}