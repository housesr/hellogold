package com.housesr.hellogold.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class SpotPriceResponse(
    @SerializedName("data")
    val data: Data?,
    @SerializedName("result")
    val result: String?
) {

    data class Data(
        val id: String = UUID.randomUUID().toString(),
        @SerializedName("buy")
        val buy: Double?,
        @SerializedName("sell")
        val sell: Double?,
        @SerializedName("spot_price")
        val spotPrice: Double?,
        @SerializedName("timestamp")
        val timestamp: String?
    )
}