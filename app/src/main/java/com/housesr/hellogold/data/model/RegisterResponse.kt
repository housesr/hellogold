package com.housesr.hellogold.data.model

import com.google.gson.annotations.SerializedName

data class RegisterResponse(
    @SerializedName("code")
    val code: String?,
    @SerializedName("data")
    val data: Data?,
    @SerializedName("result")
    val result: String?
) {

    data class Data(
        @SerializedName("account_number")
        val accountNumber: String?,
        @SerializedName("api_key")
        val apiKey: String?,
        @SerializedName("api_token")
        val apiToken: String?,
        @SerializedName("public_key")
        val publicKey: String?
    )
}