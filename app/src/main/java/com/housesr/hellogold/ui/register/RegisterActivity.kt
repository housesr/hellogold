package com.housesr.hellogold.ui.register

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import com.housesr.hellogold.R
import com.housesr.hellogold.ui.dashboard.DashboardActivity
import com.housesr.hellogold.util.EventObserver
import com.housesr.hellogold.util.Injector
import com.housesr.hellogold.util.alert
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        viewModel = ViewModelProviders.of(this, Injector.provideRegisterViewModelFactory()).get()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        btnRegister.setOnClickListener { onRegisterButtonClicked() }

        viewModel.isLoading.observe(this, Observer { onLoadingChanged(it) })
        viewModel.registerSuccess.observe(this, EventObserver { onRegisterSuccess(it) })
        viewModel.registerError.observe(this, EventObserver { onRegisterError(it) })
    }

    private fun onLoadingChanged(isLoading: Boolean) {
        progressBar.isVisible = isLoading
        btnRegister.isEnabled = !isLoading
    }

    private fun onRegisterButtonClicked() {
        val email = etEmail.text.toString()
        val tnc = cbTnc.isChecked
        viewModel.onRegisterButtonClicked(email, tnc)
    }

    private fun onRegisterSuccess(email: String) =
        DashboardActivity.getStartIntent(this, email).also { startActivity(it) }


    private fun onRegisterError(msg: String) {
        alert {
            setMessage(msg)
            setPositiveButton(android.R.string.ok) { _, _ -> }
        }
    }
}
