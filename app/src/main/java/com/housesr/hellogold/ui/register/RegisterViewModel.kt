package com.housesr.hellogold.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.housesr.hellogold.data.model.RegisterResponse
import com.housesr.hellogold.data.repository.HelloGoldRepository
import com.housesr.hellogold.ui.base.BaseViewModel
import com.housesr.hellogold.util.Event
import com.housesr.hellogold.util.HelloGoldKeyGenerator
import com.housesr.hellogold.util.isEmail
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.zipWith
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

class RegisterViewModel(private val repository: HelloGoldRepository) : BaseViewModel() {

    private val _registerSuccess = MutableLiveData<Event<String>>()
    val registerSuccess: LiveData<Event<String>> = _registerSuccess

    private val _registerError = MutableLiveData<Event<String>>()
    val registerError: LiveData<Event<String>> = _registerError

    fun onRegisterButtonClicked(email: String, tnc: Boolean) {
        if (isValid(email)) register(email, tnc)
    }

    private fun isValid(email: String): Boolean =
        when {
            email.isEmpty() -> {
                _registerError.value = Event("Email must not be empty")
                false
            }
            !email.isEmail() -> {
                _registerError.value = Event("Invalid email format")
                false
            }
            else -> true
        }

    private fun register(email: String, tnc: Boolean) {
        repository.register(
            email,
            UUID.randomUUID().toString(),
            HelloGoldKeyGenerator.generate256BitString() ?: "",
            tnc
        )
            .zipWith(Single.just(email))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _isLoading.value = true }
            .doFinally { _isLoading.value = false }
            .subscribeBy(
                onSuccess = { onRegisterSuccess(it) },
                onError = { onRegisterError(it) }
            )
            .addTo(compositeDisposable)
    }

    private fun onRegisterSuccess(response: Pair<RegisterResponse, String>) {
        if (response.first.result == "ok") _registerSuccess.value = Event(response.second)
        else _registerError.value = Event("Error:${response.first.code.toString()}")
    }

    private fun onRegisterError(throwable: Throwable) {
        Timber.e(throwable)
        throwable.message?.let { _registerError.value = Event(it) }
    }
}