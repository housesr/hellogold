package com.housesr.hellogold.ui.dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.housesr.hellogold.R
import com.housesr.hellogold.data.model.SpotPriceResponse
import com.housesr.hellogold.util.EventObserver
import com.housesr.hellogold.util.Injector
import com.housesr.hellogold.util.alert
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    private lateinit var viewModel: DashboardViewModel
    private lateinit var listAdapter: PriceListAdapter
    private lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        viewModel = ViewModelProviders.of(this, Injector.provideDashboardViewModelFactory()).get()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        tvEmail.text = intent.getStringExtra(EMAIL)

        layoutManager = LinearLayoutManager(this)
        listAdapter = PriceListAdapter()
        recyclerView.apply {
            addItemDecoration(DividerItemDecoration(this@DashboardActivity, RecyclerView.VERTICAL))
            layoutManager = this@DashboardActivity.layoutManager
            adapter = listAdapter
        }

        swipeRefreshLayout.setOnRefreshListener { viewModel.spotPrice() }
        btnRefresh.setOnClickListener { viewModel.spotPrice() }

        viewModel.isLoading.observe(this, Observer { onLoadingChanged(it) })
        viewModel.priceList.observe(this, Observer { onPriceListChanged(it) })
        viewModel.spotPriceError.observe(this, EventObserver { onSpotPriceError(it) })
    }

    private fun onLoadingChanged(isLoading: Boolean) {
        swipeRefreshLayout.isRefreshing = isLoading
        btnRefresh.isEnabled = !isLoading
    }

    private fun onPriceListChanged(list: List<SpotPriceResponse.Data>) {
        listAdapter.submitList(list)
        recyclerView.postOnAnimation { layoutManager.smoothScrollToPosition(recyclerView, null, 0) }
    }

    private fun onSpotPriceError(msg: String) {
        alert {
            setMessage(msg)
            setPositiveButton(android.R.string.ok) { _, _ -> }
        }
    }

    companion object {

        private const val EMAIL = "email"

        fun getStartIntent(context: Context, email: String) =
            Intent(context, DashboardActivity::class.java)
                .apply { putExtra(EMAIL, email) }
    }
}