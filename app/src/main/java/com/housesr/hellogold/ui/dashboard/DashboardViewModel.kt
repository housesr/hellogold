package com.housesr.hellogold.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.housesr.hellogold.data.model.SpotPriceResponse
import com.housesr.hellogold.data.repository.HelloGoldRepository
import com.housesr.hellogold.ui.base.BaseViewModel
import com.housesr.hellogold.util.Event
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.zipWith
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class DashboardViewModel(private val repository: HelloGoldRepository) : BaseViewModel() {

    private val _priceList = MutableLiveData<List<SpotPriceResponse.Data>>()
    val priceList: LiveData<List<SpotPriceResponse.Data>> = _priceList

    private val _spotPriceError = MutableLiveData<Event<String>>()
    val spotPriceError: LiveData<Event<String>> = _spotPriceError

    init {
        spotPrice()
    }

    fun spotPrice() {
        repository.spotPrice()
            .zipWith(Single.just(_priceList.value ?: listOf()))
            .map { pair ->
                val priceList = pair.second.toMutableList()
                pair.first.data?.let { priceList.add(0, it) }
                priceList
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _isLoading.value = true }
            .doFinally { _isLoading.value = false }
            .subscribe(
                { onSpotPriceSuccess(it) },
                { onSpotPriceError(it) }
            )
            .addTo(compositeDisposable)
    }

    private fun onSpotPriceSuccess(data: MutableList<SpotPriceResponse.Data>) {
        _priceList.value = data
    }

    private fun onSpotPriceError(throwable: Throwable?) {
        Timber.e(throwable)
        throwable?.message?.let { _spotPriceError.value = Event(it) }
    }
}