package com.housesr.hellogold.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.housesr.hellogold.R
import com.housesr.hellogold.data.model.SpotPriceResponse
import com.housesr.hellogold.util.PriceDiffCallback

class PriceListAdapter : ListAdapter<SpotPriceResponse.Data, PriceListAdapter.PriceViewHolder>(PriceDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PriceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return inflater.inflate(R.layout.item_price, parent, false)
            .run { PriceViewHolder(this) }
    }

    override fun onBindViewHolder(holder: PriceViewHolder, position: Int) {
        holder.setData(getItem(position))
    }

    class PriceViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val context = view.context

        private val tvBuy: TextView = view.findViewById(R.id.tvBuy)
        private var tvSell: TextView = view.findViewById(R.id.tvSell)
        private var tvSpotPrice: TextView = view.findViewById(R.id.tvSpotPrice)
        private var tvTimestamp: TextView = view.findViewById(R.id.tvTimestamp)

        fun setData(data: SpotPriceResponse.Data) {
            tvBuy.text = context.getString(R.string.placeholder_buy, data.buy)
            tvSell.text = context.getString(R.string.placeholder_sell, data.sell)
            tvSpotPrice.text = context.getString(R.string.placeholder_spot_price, data.spotPrice)
            tvTimestamp.text = context.getString(R.string.placeholder_timestamp, data.timestamp)
        }
    }
}