package com.housesr.hellogold.util

import android.util.Patterns
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity

inline fun FragmentActivity.alert(build: AlertDialog.Builder.() -> Unit): AlertDialog {
    val dialog = AlertDialog.Builder(this)
        .run {
            build()
            create()
        }
    dialog.show()
    return dialog
}

fun String?.isEmail(): Boolean = Patterns.EMAIL_ADDRESS.matcher(this).matches()