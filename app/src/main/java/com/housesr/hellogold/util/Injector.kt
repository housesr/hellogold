package com.housesr.hellogold.util

import com.housesr.hellogold.data.repository.HelloGoldRepository
import com.housesr.hellogold.data.source.HelloGoldService
import com.housesr.hellogold.ui.dashboard.DashboardViewModelFactory
import com.housesr.hellogold.ui.register.RegisterViewModelFactory

object Injector {

    fun provideRegisterViewModelFactory(): RegisterViewModelFactory {
        val service = HelloGoldService.getInstance()
        val repository = HelloGoldRepository(service)
        return RegisterViewModelFactory(repository)
    }

    fun provideDashboardViewModelFactory(): DashboardViewModelFactory {
        val service = HelloGoldService.getInstance()
        val repository = HelloGoldRepository(service)
        return DashboardViewModelFactory(repository)
    }
}