package com.housesr.hellogold.util

import androidx.recyclerview.widget.DiffUtil
import com.housesr.hellogold.data.model.SpotPriceResponse

class PriceDiffCallback : DiffUtil.ItemCallback<SpotPriceResponse.Data>() {

    override fun areItemsTheSame(oldItem: SpotPriceResponse.Data, newItem: SpotPriceResponse.Data): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: SpotPriceResponse.Data, newItem: SpotPriceResponse.Data): Boolean {
        return oldItem == newItem
    }
}