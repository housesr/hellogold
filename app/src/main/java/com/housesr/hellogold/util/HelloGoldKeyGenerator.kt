package com.housesr.hellogold.util

import android.util.Base64
import javax.crypto.KeyGenerator

object HelloGoldKeyGenerator {

    fun generate256BitString(): String? {
        val keyGen = KeyGenerator.getInstance("AES").apply {
            init(256)
        }
        return Base64.encodeToString(keyGen.generateKey().encoded, Base64.DEFAULT)
    }
}